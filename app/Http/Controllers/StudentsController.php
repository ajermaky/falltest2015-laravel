<?php

namespace App\Http\Controllers;

use App\College;
use App\Student;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $students = Student::all();
        $json = array();
        foreach($students as $student){
            array_push($json,array("first_name"=>$student->first_name,"last_name"=>$student->last_name,'college'=>$student->colleges()->first()->name));
        }

        return response()->json($json);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = $request->all();
        $student = new Student;
        $student->first_name=$input['first_name'];
        $student->last_name=$input['last_name'];
        $college = College::where('name','=',$input['college'])->first();
        if(!($student->colleges()->associate($college))){
            return response()->json(array('status'=>'error'));
        }
        if($student->save()){
            return response()->json(array('status'=>'ok'));

        }
        return response()->json(array('status'=>'error'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $student = Student::find($id);
        return response()->json(array('first_name'=>$student->first_name,'last_name'=>$student->last_name,'college'=>$student->colleges()->first()->name));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input = $request->all();
        $student = Student::find($id);
        $student->first_name=$input['first_name'];
        $student->last_name=$input['last_name'];
        $college = College::where('name','=',$input['college'])->first();
        if(!($student->colleges()->associate($college))){
            return response()->json(array('status'=>'error'));
        }
        if($student->save()){
            return response()->json(array('status'=>'ok'));

        }
        return response()->json(array('status'=>'error'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(Student::destroy($id)){
            return response()->json(array('status'=>'ok'));
        }

        return response()->json(array('status'=>'error'));
    }
}
