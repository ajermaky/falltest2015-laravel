<?php

namespace App\Http\Controllers;

use App\Address;
use App\Student;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AddressesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $addresses = Address::all();
        $json = array();
        foreach($addresses as $address){
            $student = $address->students()->first();
            $studentjson=array('first_name'=>$student->first_name,'last_name'=>$student->last_name);
            array_push($json,array("street_name"=>$address->street_name,"city"=>$address->city,"state"=>$address->state,
                "zip_code"=>$address->zip_code,'student'=>$studentjson));
        }

        return response()->json($json);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = $request->all();
        $address = new Address;
        $student = Student::where('first_name','=',$input['student']['first_name'])->where('last_name','=',$input['student']['last_name'])->first();
        $address->street_name=$input['street_name'];
        $address->city= $input['city'];
        $address->state = $input['state'];
        $address->zip_code = $input['zip_code'];
        if(!($address->students()->associate($student))){
            return response()->json(array('status'=>'error'));

        }
        if($address->save()){
            return response()->json(array('status'=>'ok'));
        }

        return response()->json(array('status'=>'error'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $address = Address::find($id);
        $student = $address->students()->first();
        $studentjson=array('first_name'=>$student->first_name,'last_name'=>$student->last_name);
        array_push($json,array("street_name"=>$address->street_name,"city"=>$address->city,"state"=>$address->state,
            "zip_code"=>$address->zip_code,'student'=>$studentjson));


        return response()->json($json);

}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input = $request->all();
        $address = Address::find($id);
        $student = Student::where('first_name','=',$input['student']['first_name'])->where('last_name','=',$input['student']['last_name']);
        $address->street_name=$input['street_name'];
        $address->city= $input['city'];
        $address->state = $input['state'];
        $address->zip_code = $input['zip_code'];
        if(!($address->students()->associate($student))){
            return response()->json(array('status'=>'error'));

        }

        if($address->save()){
            return response()->json(array('status'=>'ok'));
        }

        return response()->json(array('status'=>'error'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(Address::destroy($id)){
            return response()->json(array('status'=>'ok'));
        }

        return response()->json(array('status'=>'error'));
    }
}
