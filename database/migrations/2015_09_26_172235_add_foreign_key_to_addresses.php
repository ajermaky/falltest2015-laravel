<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToAddresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('addresses', function (Blueprint $table) {
            //
            $table->integer("students_id")->unsigned();
            $table->foreign("students_id")->references("id")->on("students");
        });
        Schema::table('students', function (Blueprint $table) {
            //
            $table->integer("colleges_id")->unsigned();
            $table->foreign("colleges_id")->references("id")->on("colleges");
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('addresses', function (Blueprint $table) {
            //
        });
    }
}
